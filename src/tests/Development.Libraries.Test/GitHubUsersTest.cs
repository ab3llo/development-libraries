﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Development.Libraries.Rest;
using Rest;
using Xunit;

namespace Development.Libraries.Test
{
	public class GitHubUsersTest
	{
		const string basePath = "https://api.github.com";

		[Fact]
		public async Task GetUsersTestAsync()
		{
			GithubApiClient api = new GithubApiClient(basePath);
			Dictionary<string, string> headers = new Dictionary<string, string>
			{
				{ "User-Agent", "Chrome" }
			};

			ApiResponse<List<GitHubModel>> response = await api.GetUsers(headers);

			Assert.True(response.IsSuccessStatusCode);
			Assert.NotNull(response.Data);
		}
	}
}
