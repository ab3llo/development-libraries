﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Development.Libraries.Rest;
using Xunit;

namespace Development.Libraries.Test
{
	public class FinBankAccountTests
	{
		const string basePath = "http://localhost:5000";

		[Fact]
		public async Task CreateABankAccountAsUserLoggedIn()
		{
			AccountApiClient accountApi = new AccountApiClient(basePath);
			ApiResponse<Account> accountResponse = await accountApi.LoginAsync(new Login { Email = "testUser@outlook.com", Password = "Password123!" });

			BankAccountApiClient client = new BankAccountApiClient(basePath);

			Dictionary<string, string> headers = new Dictionary<string, string>
			{
				{ "Authorization", accountResponse.Data.Token }
			};

			ApiResponse<Bank.Account> response = await client.CreateBankAccountAsync(new Bank.Account { Name = "Monzo", Description = "Monthly budget!", Amount="500"},headers);

			Assert.True(response.IsSuccessStatusCode);
			Assert.NotNull(response.Data);
		}

		[Fact]
		public async Task GetABankAccountAsUserLoggedIn()
		{
			AccountApiClient accountApi = new AccountApiClient(basePath);
			ApiResponse<Account> accountResponse = await accountApi.LoginAsync(new Login { Email = "testUser@outlook.com", Password = "Password123!" });
			Bank.Account bankAccount = new Bank.Account()
			{
				Name = "Monzo",
				Description = "Monthly budget!",
				Amount = "500.00"
			};

			BankAccountApiClient client = new BankAccountApiClient(basePath);

			Dictionary<string, string> headers = new Dictionary<string, string>
			{
				{ "Authorization", accountResponse.Data.Token }
			};

			ApiResponse<Bank.Account> response = await client.CreateBankAccountAsync(bankAccount, headers);

			Assert.True(response.IsSuccessStatusCode);
			Assert.NotNull(response.Data);

			bankAccount.Id = response.Data.Id;

			response = await client.GetBankAccountAsync(bankAccount.Id, headers);

			Assert.True(response.IsSuccessStatusCode);

			Bank.Account actual = response.Data;
			bankAccount.DateCreated = response.Data.DateCreated;

			Assert.NotNull(response.Data);
		}


		[Fact]
		public async Task UpdateABankAccountAsUserLoggedIn()
		{
			AccountApiClient accountApi = new AccountApiClient(basePath);
			ApiResponse<Account> accountResponse = await accountApi.LoginAsync(new Login { Email = "testUser@outlook.com", Password = "Password123!" });
			Bank.Account bankAccount = new Bank.Account()
			{
				Name = "Monzo",
				Description = "Monthly budget!",
				Amount = "500"
			};

			BankAccountApiClient client = new BankAccountApiClient(basePath);

			Dictionary<string, string> headers = new Dictionary<string, string>
			{
				{ "Authorization", accountResponse.Data.Token }
			};

			ApiResponse<Bank.Account> response = await client.CreateBankAccountAsync(bankAccount, headers);

			Assert.True(response.IsSuccessStatusCode);
			Assert.NotNull(response.Data);

			Bank.Account newBankAccount = new Bank.Account()
			{
				Name = "Monzo update",
				Description = "Update!",
				Amount = "900.0"
			};

			newBankAccount.DateCreated = response.Data.DateCreated;
			response = await client.UpdateBankAccountAsync(response.Data.Id, newBankAccount, headers);

			Assert.Equal(HttpStatusCode.OK, response.Status);
			Assert.NotNull(response.Data);

			newBankAccount.Id = response.Data.Id;
			Assert.NotEqual(newBankAccount.Name, bankAccount.Name);
			Assert.NotEqual(newBankAccount.Description, bankAccount.Description);
			Assert.NotEqual(newBankAccount.Amount, bankAccount.Amount);
		}


		[Fact]
		public async Task DeleteABankAccountAsUserLoggedIn()
		{
			AccountApiClient accountApi = new AccountApiClient(basePath);
			ApiResponse<Account> accountResponse = await accountApi.LoginAsync(new Login { Email = "testUser@outlook.com", Password = "Password123!" });
			Bank.Account bankAccount = new Bank.Account()
			{
				Name = "Monzo",
				Description = "Monthly budget!",
				Amount = "500"
			};

			BankAccountApiClient client = new BankAccountApiClient(basePath);

			Dictionary<string, string> headers = new Dictionary<string, string>
			{
				{ "Authorization", accountResponse.Data.Token }
			};

			ApiResponse<Bank.Account> response = await client.CreateBankAccountAsync(new Bank.Account { Name = "Monzo", Description = "Monthly budget!", Amount = "500" }, headers);

			Assert.True(response.IsSuccessStatusCode);
			Assert.NotNull(response.Data);

			response = await client.DeleteBankAccountAsync(response.Data.Id, headers);
			Assert.Equal(HttpStatusCode.NoContent, response.Status);
			Assert.Null(response.Data);
		}
	}
}
