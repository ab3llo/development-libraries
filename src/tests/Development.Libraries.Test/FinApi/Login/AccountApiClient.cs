﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Development.Libraries.Rest;

namespace Development.Libraries.Test
{
	public class AccountApiClient:RestClient
	{
		private string url;

		public AccountApiClient(string basePath): base(basePath)
		{
			url = basePath + "/Account";
		}

		public async Task<ApiResponse<Account>> LoginAsync(object body, Dictionary<string,string> headers = null, string queryParameters = null)
		{
			Uri uri = new Uri($"{url}/Login" + queryParameters);

			var response = await PostAsync<Account>(uri, body, headers);

			return response;
		}
	}
}
