﻿namespace Development.Libraries.Test
{
	public class Account
	{
		public string Id { get; set; }

		public string UserName { get; set; }

		public string Token { get; set; }
	}
}