﻿using Newtonsoft.Json;

namespace Development.Libraries.Test.Bank
{
	public class Account
	{
		public string Name { get; set; }

		public string Description { get; set; }

		public string Amount { get; set; }

		[JsonProperty( NullValueHandling = NullValueHandling.Ignore)]
		public string Id { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string DateCreated { get; set; }
	}
}
