﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Development.Libraries.Rest;

namespace Development.Libraries.Test
{
	public class BankAccountApiClient : RestClient
	{
		public BankAccountApiClient(string basePath):base(basePath)
		{
		}

		public async Task<ApiResponse<Bank.Account>> CreateBankAccountAsync(object body, Dictionary<string, string> headers = null, string queryParameters = null)
		{
			Uri uri = new Uri($"{BasePath}/api/bankaccount" + queryParameters);

			var response = await PostAsync<Bank.Account>(uri, body, headers);
			return response;
		}

		public async Task<ApiResponse<Bank.Account>> UpdateBankAccountAsync(string id, object body, Dictionary<string, string> headers = null, string queryParameters = null)
		{
			Uri uri = new Uri($"{BasePath}/api/bankaccount/{id}" + queryParameters);

			var response = await PutAsync<Bank.Account>(uri, body, headers);
			return response;
		}

		public async Task<ApiResponse<Bank.Account>> GetBankAccountAsync(string id, Dictionary<string, string> headers = null, string queryParameters = null )
		{
			Uri uri = new Uri($"{BasePath}/api/bankaccount/{id}" + queryParameters);

			var response = await GetAsync<Bank.Account>(uri,headers);
			return response;
		}

		public async Task<ApiResponse<Bank.Account>> DeleteBankAccountAsync(string id, Dictionary<string, string> headers = null, string queryParameters = null)
		{
			Uri uri = new Uri($"{BasePath}/api/bankaccount/{id}" + queryParameters);

			var response = await DeleteAsync<Bank.Account>(uri, headers);
			return response;
		}
	}
}
