using System.Net;
using System.Threading.Tasks;
using Development.Libraries.Rest;
using Xunit;

namespace Development.Libraries.Test
{
	public class FinLoginTests
	{
		const string basePath = "http://localhost:5000";
		[Fact]
		public async Task UserSuccesfullyLoginToFin()
		{
			AccountApiClient client = new AccountApiClient(basePath);
			ApiResponse<Account> response = await client.LoginAsync(new Login { Email = "testUser@outlook.com", Password = "Password123!" });

			Assert.True(response.IsSuccessStatusCode);
			Assert.NotNull(response.Data);
		}

		[Fact]
		public async Task UserFailsToLoginWithInvalidPassword()
		{
			AccountApiClient client = new AccountApiClient(basePath);
			ApiResponse<Account> response = await client.LoginAsync(new Login { Email = "testUser@outlook.com", Password = "FakePassword!" });

			Assert.Equal(HttpStatusCode.NotFound,response.Status);
		}

		[Fact]
		public async Task UserThatDoesNotExistFailsToLogin()
		{
			AccountApiClient client = new AccountApiClient(basePath);
			ApiResponse<Account> response = await client.LoginAsync(new Login { Email = "fakeUser@outlook.com", Password = "Password123!" });

			Assert.Equal(HttpStatusCode.NotFound, response.Status);
		}
	}
}
