﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Development.Libraries.API.Models;

namespace Development.Libraries.API.Repositories
{
	public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseModel
	{
		private readonly DbContext _dbContext;

		public GenericRepository(DbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task Create(TEntity entity)
		{
			entity.DateCreated = DateTime.UtcNow;
			await _dbContext.Set<TEntity>().AddAsync(entity);
			await _dbContext.SaveChangesAsync();
		}

		public async Task Delete(Guid id)
		{
			var entity = await Get(id);
			_dbContext.Set<TEntity>().Remove(entity);
			await _dbContext.SaveChangesAsync();
		}

		public IEnumerable<TEntity> GetAll()
		{
			return _dbContext.Set<TEntity>().AsNoTracking();
		}

		public async Task<TEntity> Get(Guid id)
		{
			return await _dbContext.Set<TEntity>()
				.AsNoTracking()
				.FirstOrDefaultAsync(e => e.Id == id);
		}

		public async Task Update(TEntity entity)
		{
			_dbContext.Set<TEntity>().Update(entity);
			await _dbContext.SaveChangesAsync();
		}
	}

}
