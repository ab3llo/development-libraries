﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Development.Libraries.API.Models;

namespace Development.Libraries.API
{
	public interface IGenericRepository<TEntity> where TEntity : BaseModel
	{
		IEnumerable<TEntity> GetAll();

		Task<TEntity> Get(Guid id);

		Task Create(TEntity entity);

		Task Update(TEntity entity);

		Task Delete(Guid id);
	}
}
